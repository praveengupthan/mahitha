 <header class="fixed-top">
        <!-- top header -->
        <div class="top-header">
            <!-- container -->
            <div class="container">
                <div class="row justify-content-end text-right">
                    <div class="col-lg-6 text-right">
                        <ul class="float-right">
                            <li class="dmnone" ><a>info@mahithawebcol.com</a></li>
                            <li><a>+91 9642123254</a></li>
                            <li><a href="https://www.facebook.com/"><span class="icon-facebook icomoon"></span></a></li>
                            <li><a href="https://www.linkedin.com/"><span class="icon-linkedin-logo icomoon"></span></a></li>
                            <li><a href="https://plus.google.com/discover"><span class="icon-google-plus1 icomoon"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/ container-->
        </div>
        <!--/ top header-->
        <!-- middle header -->
        <div class="middle-header">
             <div class="container px-0">                
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="index.php"><img src="img/logo.svg" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="about.php">About us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="courses.php">Courses</a>
                            </li>                             
                            <li class="nav-item">
                                <a class="nav-link" href="contact.php">Contact</a>
                            </li> 
                            <li class="nav-item">
                                <a class="nav-link newlink" href="javascript:void(0)" data-toggle="modal" data-target="#myModal2">Get Appointment</a>
                            </li>                                     
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--/ middle header -->
    </header>

    