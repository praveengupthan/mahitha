<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Courses at Mahitha Web Solutions Traning and Support</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- sub page -->
    <div class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="breadcumb-overlay"></div>
             <!-- container -->
             <div class="container">
                 <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                         <article class="header-page">
                             <h1>PHP Development</h1>
                             <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat, quis.</p>
                         </article>
                    </div>
                 </div>
             </div>
             <!--/ container -->
        </div>
        <!--/ sub page header -->
        <!-- sub page main-->
        <div class="subpage-main">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left column -->
                    <div class="col-lg-9">
                        <div class="coursedetail">
                             <h2 class="coursetitle">Improve Web Skills &amp;  Workflow with PHP Development</h2>
                             <ul class="nav prdetails">
                                <li class="list-item">
                                    <p><small>Faculty</small></p>
                                    <h3>Venkatesh</h3>
                                </li>
                                <li class="list-item">
                                    <p><small>Category</small></p>
                                    <h3>Technologies</h3>
                                </li>
                                <li class="list-item">
                                    <p><small>Course Name</small></p>
                                    <h3>PHP Development</h3>
                                </li>
                                <li class="list-item">
                                    <p><small>Download</small></p>
                                    <h3><a href="img/dummy.pdf" class="linkanchor" download><span class="icon-download1 icomoon"></span>Download Full Course</a></h3>
                                </li>
                             </ul>
                             <figure class="coursedetailbanner">
                                 <img src="img/coursedetailimg.jpg" alt="" title="" class="img-fluid">
                             </figure>
                        </div>

                        <!-- course detail tab-->
                        <div class="parentHorizontalTab coursetab">
                            <ul class="resp-tabs-list hor_1">
                                <li>Overview</li>
                                <li>Curriculum</li>
                                <li>Instructor</li>
                            </ul>
                            <div class="resp-tabs-container hor_1">

                                <!-- over view -->
                                <div>
                                    <h3>Course Description</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                                    <h3>Certification</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                                    <h3>Learning Outcomes</h3>
                                    <ul class="listitems pb-2">
                                        <li>Over 37 lectures and 55.5 hours of content!</li>

                                        <li>LIVE PROJECT End to End Software Testing Training Included.</li>

                                        <li>Learn Software Testing and Automation basics from a professional trainer from your own desk.</li>

                                        <li>Information packed practical training starting from basics to advanced testing techniques.</li>

                                        <li>Best suitable for beginners to advanced level users and who learn faster when demonstrated.</li>

                                        <li>Course content designed by considering current software testing technology and the job market.</li>

                                        <li>Practical assignments at the end of every session.</li>

                                        <li>Practical learning experience with live project work and examples.</li> 
                                    </ul>


                                </div>
                                <!--/ over view -->

                                <!-- curriculum-->
                                <div>               
                                    <!-- accordian -->
                                    <div class="accordian-tab">
                                        <div class="accordion">
                                            <h3 class="panel-title">1. Module Name will be here</h3>
                                            <div class="panel-content">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td>Lecture1.1</td>
                                                        <td>How to Install</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 1.2</td>
                                                        <td>The Basics of Variables in Sass</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 1.3</td>
                                                        <td>Nesting Your Sass</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 1.4</td>
                                                        <td>Using Mixins </td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 1.5</td>
                                                        <td>Selector Inheritance</td>
                                                        <td>30 Min</td>
                                                    </tr>                                                   
                                                </table>
                                            </div>
                                            <h3 class="panel-title">2. Module Name will be here</h3>
                                            <div class="panel-content">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td>Lecture 2.1</td>
                                                        <td>How to Install</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 2.2</td>
                                                        <td>The Basics of Variables in Sass</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 2.3</td>
                                                        <td>Nesting Your Sass</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 2.4</td>
                                                        <td>Using Mixins </td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 2.5</td>
                                                        <td>Selector Inheritance</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>Lecture 2.6</td>
                                                        <td>Build Your Own Functions</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 2.7</td>
                                                        <td>Build Your Own Function Part 2 – If Else  </td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <h3 class="panel-title">3. Module Name will be here</h3>
                                            <div class="panel-content">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td>Lecture 3.1</td>
                                                        <td>How to Install</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 3.2</td>
                                                        <td>The Basics of Variables in Sass</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 3.3</td>
                                                        <td>Nesting Your Sass</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 3.4</td>
                                                        <td>Using Mixins </td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 3.5</td>
                                                        <td>Selector Inheritance</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>Lecture 3.6</td>
                                                        <td>Build Your Own Functions</td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lecture 3.7</td>
                                                        <td>Build Your Own Function Part 2 – If Else  </td>
                                                        <td>30 Min</td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                    <!--/ accordian --> 
                                </div>
                                <!--/ curriculum-->

                                <!-- faculty -->
                                <div>
                                    <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-2">
                                            <img src="img/testimg01.jpg" alt="" title="" class="img-fluid">
                                        </div>
                                        <!--/ col -->
                                        <!--col -->
                                        <div class="col-lg-9 align-self-center ">
                                            <h5 class="h5 pb-0 mb-0">Faculty name will be here</h5>
                                            <p class="pb-0"><small>Senior Software Engineer / PHP Developer</small></p>
                                            <p><small>12 Years Experience</small></p>
                                        </div>
                                        <!--/ col -->
                                        <!-- col -->
                                        <div class="col-lg-12 py-2">
                                            <p>Keny White is Professor of the Department of Computer Science at Boston University, where he has been since 2004. He also currently serves as Chief Scientist of Guavus, Inc. During 2003-2004 he was a Visiting Associate Professor at the Laboratoire d'Infomatique de Paris VI (LIP6). He received a B.S. from Cornell University in 1992, and an M.S. from the State University of New York at Buffalo.</p>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->
                                </div>
                                <!--/ faculty-->
                            </div>
                        </div>
                        <!--/ course detail tab -->
                    </div>
                    <!--/ left column -->
                    <!-- right column -->
                    <div class="col-lg-3 rtcourse">
                        <h4 class="h4 flight">All Courses</h4>
                        <ul class="rightlist">
                            <li><a href="javascript:void(0)">PHP Development</a></li>
                            <li><a href="javascript:void(0)">Ajax Full course</a></li>
                            <li><a href="javascript:void(0)">Jquery Development</a></li>
                            <li><a href="javascript:void(0)">Software Development</a></li>
                            <li><a href="javascript:void(0)">Programing Language</a></li>
                        </ul>
                    </div>
                    <!--/ right column -->
                </div>
                <!--/ row -->               
            </div>
             <!-- students testimonials -->
    <div class="home-testimonials">
        <!-- container -->
        <div class="container">
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-lg-8 text-center">
                    <article class="title-home py-3">                       
                        <h5 class="sectiontitle">Studnets  <span>Testimonials</span></h5>
                    </article>

                    <!-- carousel -->
                    <div id="testimonials-car" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <!-- item -->
                            <div class="carousel-item active">
                                <!-- image -->
                                <img src="img/testimonialimg.jpg" alt="">
                                <h4>Student Name will be here</h4>
                                <p class="desi">Software Engineer, Name of Company</p>
                                <p class="py-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing .</p>
                                <!--/ image -->
                            </div>
                            <!--/ item -->
                            <!-- item -->
                            <div class="carousel-item">
                                    <!-- image -->
                                    <img src="img/testimonialimg.jpg" alt="">
                                    <h4>Student Name will be here</h4>
                                    <p class="desi">Software Engineer, Name of Company</p>
                                    <p class="py-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing .</p>
                                    <!--/ image -->
                                </div>
                                <!--/ item -->
                                <!-- item -->
                            <div class="carousel-item">
                                    <!-- image -->
                                    <img src="img/testimonialimg.jpg" alt="">
                                    <h4>Student Name will be here</h4>
                                    <p class="desi">Software Engineer, Name of Company</p>
                                    <p class="py-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing .</p>
                                    <!--/ image -->
                                </div>
                                <!--/ item -->
                        </div>
                        <a class="carousel-control-prev" href="#testimonials-car" role="button" data-slide="prev">
                            <span class="icon-left-arrow icomoon"></span>
                        </a>
                        <a class="carousel-control-next" href="#testimonials-car" role="button" data-slide="next">
                            <span class="icon-right-arrow icomoon"></span>
                        </a>
                    </div>
                    <!--/ carousel-->
                </div>
                <!--/ col -->
            </div>
        </div>
        <!--/ container -->
    </div>
    <!--/ students testimonials -->  
        </div>
        <!--/sub page main -->
    </div>
    <!--/ sub page -->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'footerscripts.php' ?>
</body>
</html>