<footer>
    <div class="container">
        <div class="row">
            <!-- col -->
            <div class="col-lg-6 col-sm-6">
                <ul class="nav">
                    <li class="nav-item"><a href="https://www.facebook.com/" target="_blank"><span class="icon-facebook-logo icomoon"></span></a></li>
                    <li class="nav-item"><a href="https://www.linkedin.com/" target="_blank"><span class="icon-linkedin-logo icomoon"></span></a></li>
                    <li class="nav-item"><a href="https://plus.google.com/discover" target="_blank"><span class="icon-google-plus1 icomoon"></span></a></li>
                </ul>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-lg-6 col-sm-6 text-right footercontact align-self-center">
                <p><span>info@mahithawebcol.com </span> <span>+91 9642123254</span></p>
            </div>
            <!--/ col -->
        </div>
    </div>
    <a id="movetop" href="javascript:void(0)"><span class="icon-up-arrow icomoon"></span></a>
</footer>

 <!-- Modal -->
 <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>               
            </div>
            <div class="modal-body">
                 <!-- col -->
                 <div class="col-lg-12 align-self-center">
                    <article class="title-home py-3">
                        <h4>Get Touch with our councellor</h4>
                        
                    </article>

                    <form class="form-touch ">
                        <div class="form-group">                           
                            <input class="form-control" type="text" placeholder="Write your Name"> 
                        </div>
                        <div class="form-group">                           
                            <input class="form-control" type="text" placeholder="Email"> 
                        </div>
                        <div class="form-group">                           
                            <input class="form-control" type="text" placeholder="Phone Number"> 
                        </div>
                        <div class="form-group">                           
                            <textarea class="form-control" placeholder="Your message" style="height:100px;"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Submit" class="btn">
                        </div>
                    </form>
                </div>
                <!--/ col -->
            </div>
        </div>
        <!-- modal-content -->
    </div>
    <!-- modal-dialog -->
</div>
<!-- modal -->