<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mahitha Web Solutions Traning and Support</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- sub page -->
    <div class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="breadcumb-overlay"></div>
             <!-- container -->
             <div class="container">
                 <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                         <article class="header-page">
                             <h1>About us</h1>
                             <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat, quis.</p>
                         </article>
                    </div>
                 </div>
             </div>
             <!--/ container -->
        </div>
        <!--/ sub page header -->
        <!-- sub page main-->
        <div class="subpage-main">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/about2.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">

                        <article class="title-home">
                            <h2 class="subtitle-home py-1">Introduction</h2>
                            <h3 class="sectiontitle">Welcome To The Biggest Online Learning Source Of Eduka</h3>
                        </article>

                        <p class="text-justify pb-2">Alteration literature to or an sympathize mr imprudence. Of is ferrars subject as enjoyed or tedious cottage. Procuring as in resembled by in agreeable. Next long no gave mr eyes. Admiration advantages no he celebrated so pianoforte unreserved. Not its herself forming charmed amiable. Him why feebly expect future now.</p>

                        <p class="text-justify pb-2">Curiosity incommode now led smallness allowance. Favour bed assure son things yet. She consisted consulted elsewhere happiness disposing household any old the. Widow downs. Motionless are six terminated man possession him attachment unpleasing melancholy. Sir smile arose one share. No abroad in easily relied an whence lovers temper by.
                        </p>

                    </div>
                    <!--/ col-->
                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row py-4">
                    <!-- col -->
                    <div class="col-lg-4 col-sm-4 text-center pagecol">
                        <span class="icon-faculty-shield icomoon"></span>
                        <h4 class="subtitle-home">Expert Faculty</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quia aliquam molestias nulla ex nemo itaque!</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-4 col-sm-4 text-center pagecol">
                        <span class="icon-ebook icomoon"></span>
                        <h4 class="subtitle-home">Online Learning</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quia aliquam molestias nulla ex nemo itaque!</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-4 col-sm-4 text-center pagecol">
                        <span class="icon-support1 icomoon"></span>
                        <h4 class="subtitle-home">Support Post course</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quia aliquam molestias nulla ex nemo itaque!</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </div>
        <!--/sub page main -->
    </div>
    <!--/ sub page -->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'footerscripts.php' ?>
</body>
</html>