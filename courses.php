<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Courses at Mahitha Web Solutions Traning and Support</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- sub page -->
    <div class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="breadcumb-overlay"></div>
             <!-- container -->
             <div class="container">
                 <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                         <article class="header-page">
                             <h1>Courses</h1>
                             <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat, quis.</p>
                         </article>
                    </div>
                 </div>
             </div>
             <!--/ container -->
        </div>
        <!--/ sub page header -->
        <!-- sub page main-->
        <div class="subpage-main">
            <div class="container">
                <!-- row -->
                <div class="row">

                   <!-- course col -->
                   <div class="col-lg-4 col-sm-6">
                        <div class="course-col">
                            <figure>
                                <a href="coure-detail.php"><img src="img/courseimg01.jpg" alt="" class="img-fluid"></a>
                            </figure>
                            <div class="facimg row p-3">
                                <div class="col-lg-2">
                                    <img src="img/testimonialimg.jpg">
                                </div>
                                <div class="col-lg-10">
                                    <h5 class="h6 mb-0">Faculty Name will be here</h6>
                                    <span><small>Senior Software Engineer</small></span>
                                </div>
                            </div>
                            <div class="coursedet p-3">
                                <h5 class="h6 fbold">Java Programming Masterclass</h5>
                                <small>Part Time Couse</small>
                                <p class="text-justify">Would day nor ask walls known. But preserved advantage are but and certainty earnestly enjoyment.</p>
                            </div>
                            <div class="duration">
                                <p>10 Weeks <span class="float-right h5 fbold">Rs:10,000</span></p>
                            </div>
                        </div>
                   </div>
                   <!--/ course col -->

                    <!-- course col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="course-col">
                            <figure>
                                <a href="coure-detail.php"><img src="img/courseimg02.jpg" alt="" class="img-fluid"></a>
                            </figure>
                            <div class="facimg row p-3">
                                <div class="col-lg-2">
                                    <img src="img/testimonialimg.jpg">
                                </div>
                                <div class="col-lg-10">
                                    <h5 class="h6 mb-0">Faculty Name will be here</h6>
                                    <span><small>Senior Software Engineer</small></span>
                                </div>
                            </div>
                            <div class="coursedet p-3">
                                <h5 class="h6 fbold">Java Programming Masterclass</h5>
                                <small>Part Time Couse</small>
                                <p class="text-justify">Would day nor ask walls known. But preserved advantage are but and certainty earnestly enjoyment.</p>
                            </div>
                            <div class="duration">
                                <p>10 Weeks <span class="float-right h5 fbold">Rs:10,000</span></p>
                            </div>
                        </div>
                   </div>
                   <!--/ course col -->

                    <!-- course col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="course-col">
                            <figure>
                                <a href="coure-detail.php"><img src="img/courseimg03.jpg" alt="" class="img-fluid"></a>
                            </figure>
                            <div class="facimg row p-3">
                                <div class="col-lg-2">
                                    <img src="img/testimonialimg.jpg">
                                </div>
                                <div class="col-lg-10">
                                    <h5 class="h6 mb-0">Faculty Name will be here</h6>
                                    <span><small>Senior Software Engineer</small></span>
                                </div>
                            </div>
                            <div class="coursedet p-3">
                                <h5 class="h6 fbold">Java Programming Masterclass</h5>
                                <small>Part Time Couse</small>
                                <p class="text-justify">Would day nor ask walls known. But preserved advantage are but and certainty earnestly enjoyment.</p>
                            </div>
                            <div class="duration">
                                <p>10 Weeks <span class="float-right h5 fbold">Rs:10,000</span></p>
                            </div>
                        </div>
                   </div>
                   <!--/ course col -->

                    <!-- course col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="course-col">
                            <figure>
                                <a href="coure-detail.php"><img src="img/courseimg04.jpg" alt="" class="img-fluid"></a>
                            </figure>
                            <div class="facimg row p-3">
                                <div class="col-lg-2">
                                    <img src="img/testimonialimg.jpg">
                                </div>
                                <div class="col-lg-10">
                                    <h5 class="h6 mb-0">Faculty Name will be here</h6>
                                    <span><small>Senior Software Engineer</small></span>
                                </div>
                            </div>
                            <div class="coursedet p-3">
                                <h5 class="h6 fbold">Java Programming Masterclass</h5>
                                <small>Part Time Couse</small>
                                <p class="text-justify">Would day nor ask walls known. But preserved advantage are but and certainty earnestly enjoyment.</p>
                            </div>
                            <div class="duration">
                                <p>10 Weeks <span class="float-right h5 fbold">Rs:10,000</span></p>
                            </div>
                        </div>
                   </div>
                   <!--/ course col -->
                    
                </div>
                <!--/ row -->               
            </div>
        </div>
        <!--/sub page main -->
    </div>
    <!--/ sub page -->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'footerscripts.php' ?>
</body>
</html>