<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- sub page -->
    <div class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="breadcumb-overlay"></div>
             <!-- container -->
             <div class="container">
                 <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                         <article class="header-page">
                             <h1>Contact</h1>
                             <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat, quis.</p>
                         </article>
                    </div>
                 </div>
             </div>
             <!--/ container -->
        </div>
        <!--/ sub page header -->
        <!-- sub page main-->
        <div class="subpage-main">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6 text-center">
                        <article class="title-home py-3">                           
                            <h5 class="sectiontitle">Get in Touch   <span>With us</span></h5>
                            <p>hese men promptly escaped from a maximum security stockade to the Los geles the and his skipper first mate</p>
                        </article>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4 contactdetails">

                        <!-- row -->
                          <div class="row pb-2">
                              <div class="col-lg-2 col-3">
                                <span class="icon-home1 icomoon"></span>
                              </div>
                              <div class="col-lg-10 col-9 align-self-center">
                                  <p>Plot No:91, H.No:41-51/1, Phase 1, Kamala Prasanna Nagar, Allwyn Colony Phase 1, Kukatpally, Hyderabad - 500072, Telangana, India</p>
                              </div>
                          </div>
                          <!--/ row -->

                           <!-- row -->
                           <div class="row pb-2">
                              <div class="col-lg-2 col-2">
                                <span class="icon-smartphone icomoon"></span>
                              </div>
                              <div class="col-lg-10 col-9 align-self-center">
                                  <p>+91 9642123254</p>
                              </div>
                          </div>
                          <!--/ row -->

                           <!-- row -->
                           <div class="row pb-2">
                              <div class="col-lg-2 col-2">
                                <span class="icon-paper-plane1 icomoon"></span>
                              </div>
                              <div class="col-lg-10 col-9 align-self-center">
                                  <p>info@mahithawebsol.com</p>
                              </div>
                          </div>
                          <!--/ row -->
                    </div>
                    <!--/ col -->

                    <!-- right col -->
                    <div class="col-lg-8">
                        <form class="form-contactpage">
                            <div class="row">

                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name*">
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Email*">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone Number*">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Subject">
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                       <textarea class="form-control" placeholder="Write Message" style="height:100px;"></textarea>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-12">
                                    <input type="submit" class="btn" value="Submit">
                                </div>
                                <!--/ col -->
                            </div>
                        </form>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="map py-4">
                             <!-- map -->
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15220.60301544835!2d78.417462!3d17.5003156!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8ec4923edd59c736!2sCore+Web+Pro!5e0!3m2!1sen!2sin!4v1551875127658" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <!--/ map -->
                        </div>
                    </div>
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/sub page main -->
    </div>
    <!--/ sub page -->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'footerscripts.php' ?>
</body>
</html>