<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mahitha Web Solutions Traning and Support</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- main -->
    <!-- slider home -->
    <div class="slider-home">
        <div id="carouselExampleIndicators" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>               
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class="carousel-item active" style="background-image: url('img/slider-01.jpg')">
                    <div class="overlay"></div>
                    <div class="carousel-caption">                        
                        <h1 class="fbold py-1 slidertitle">Reach their goals</h1>  
                        <p class="flight">Programming Needs Complete Solutions</p>                    
                    </div>
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('img/slider-02.jpg')">
                    <div class="overlay"></div>
                    <div class="carousel-caption">                       
                        <h2 class="fbold py-1 slidertitle">Reach their goals</h2> 
                        <p class="flight">Programming Needs Complete Solutions</p>                       
                    </div>
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('img/slider-03.jpg')">
                    <div class="overlay"></div>
                    <div class="carousel-caption">                       
                        <h2 class="fbold py-1 slidertitle">Reach their goals</h2> 
                        <p class="flight">Programming Needs Complete Solutions</p>                       
                    </div>
                </div>
            </div>            
        </div>
    </div>
    <!--/ slider home -->
    <!-- services -->
    <div class="services-home">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 servicecol col-sm-6">
                    <span class="icon-training icomoon"></span>
                    <h3 class="text-uppercase fbold"><a href="javascript:void(0)">Training</a></h3>
                    <p>At TLI, we appreciate that each business has its own unique requirements. We develop and deploy a wide range of customized business solutions for our clients worldwide.  </p>
                </div>
                <!--/ -->

                 <!-- col -->
                 <div class="col-lg-4 col-sm-6 servicecol">
                    <span class="icon-support1 icomoon"></span>
                    <h3 class="text-uppercase fbold"><a href="javascript:void(0)">Support</a></h3>
                    <p>At TLI, we appreciate that each business has its own unique requirements. We develop and deploy a wide range of customized business solutions for our clients worldwide.  </p>
                </div>
                <!--/ -->

                     <!-- col -->
                <div class="col-lg-4 col-sm-6 servicecol">
                     <span class="icon-video-lecture icomoon"></span>
                    <h3 class="text-uppercase fbold"><a href="javascript:void(0)">Online Classes</a></h3>
                    <p>At TLI, we appreciate that each business has its own unique requirements. We develop and deploy a wide range of customized business solutions for our clients worldwide.  </p>
                </div>
                <!--/ -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ services -->
    <!-- about -->
    <div class="about-home">
        <!-- container -->
        <div class="container">
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 align-self-center">
                     <article class="title-home py-3">
                         <h4>About us</h4>
                         <h5 class="sectiontitle">Live up to  your  <span>logical</span>  potential.</h5>
                     </article>
                     <p class="text-justify">
                        Calling nothing end fertile for venture way boy. Esteem spirit temper too say adieus who direct esteem. It esteems luckily mr or picture placing drawing no. Apartments frequently or motionless on reasonable projecting expression. Way mrs end gave tall walk fact bed. Calling nothing end fertile for venture way boy. Esteem spirit temper too say adieus who direct esteem.
                     </p>
                     <h6 class="subtitle-home py-3">LEARNING OUTCOMES</h6>
                     <ul class="listitems pb-2">
                         <li>Apartments frequently or motionless on reasonable projecting expression. Way mrs end gave tall walk fact bed.</li>
                         <li>We assisted big players in market to find the right LAMP developer. We arrange interviews on/off campus. </li>
                         <li>Our fanatic dedication to quality and excellence allows the creation of software with high levels of efficiency.</li>
                     </ul>
                     <a href="about.php" class="link">READ MORE</a>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6">
                    <img src="img/homeaboutimg.jpg" alt="" title="" class="img-fluid">
                </div>
                <!--/ col -->
            </div>
        </div>
        <!--/ container -->
    </div>
    <!--/ about -->
    <!-- courses -->
    <div class="home-courses">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-6">
                    <article class="title-home py-3">
                        <h4>Courses</h4>
                        <h5 class="sectiontitle">Courses  <span>We Offered</span></h5>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                    </article>
                </div>
            </div>
            <!--/ row -->
            <!-- row -->
            <div class="row py-4">
                <!-- col -->
                <div class="col-lg-3 col-sm-6 coursecol">
                    <a href="coure-detail.php"><img src="img/phplogo.png" alt="" title="" class="img-fluid"></a>
                     <article>
                         <h5 class="subtitle-home py-2">PHP Programming</h5>
                         <p>In this course we deal with Client-Side as Well Server-Side web technologies. </p>
                     </article>
                </div>
                <!--/ col-->
                 <!-- col -->
                 <div class="col-lg-3 col-sm-6 coursecol">
                 <a href="coure-detail.php"><img src="img/pythonlogo.png" alt="" title="" class="img-fluid"></a>
                        <article>
                            <h5 class="subtitle-home py-2">Phython</h5>
                            <p>In this course we deal with Client-Side as Well Server-Side web technologies. </p>
                        </article>
                </div>
                <!--/ col-->
                 <!-- col -->
                 <div class="col-lg-3 col-sm-6 coursecol">
                    <a href="coure-detail.php"><img src="img/ajaxlogo.png" alt="" title="" class="img-fluid"></a>
                        <article>
                            <h5 class="subtitle-home py-2">Ajax Programming</h5>
                            <p>In this course we deal with Client-Side as Well Server-Side web technologies. </p>
                        </article>
                </div>
                <!--/ col-->
                 <!-- col -->
                 <div class="col-lg-3 col-sm-6 coursecol">
                 <a href="coure-detail.php"><img src="img/jquerylogo.png" alt="" title="" class="img-fluid"></a>
                    <article>
                        <h5 class="subtitle-home py-2">Jquery framework</h5>
                        <p>In this course we deal with Client-Side as Well Server-Side web technologies. </p>
                    </article>
                </div>
                <!--/ col-->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!-- courses -->
    <!-- students testimonials -->
    <div class="home-testimonials">
        <!-- container -->
        <div class="container">
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-lg-8 text-center">
                    <article class="title-home py-3">                       
                        <h5 class="sectiontitle">Studnets  <span>Testimonials</span></h5>                               
                    </article>

                    <!-- carousel -->
                    <div id="testimonials-car" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <!-- item -->
                            <div class="carousel-item active">
                                <!-- image -->
                                <img src="img/testimonialimg.jpg" alt="">
                                <h4>Student Name will be here</h4>
                                <p class="desi">Software Engineer, Name of Company</p>
                                <p class="py-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing .</p>
                                <!--/ image -->
                            </div>
                            <!--/ item -->
                            <!-- item -->
                            <div class="carousel-item">
                                    <!-- image -->
                                    <img src="img/testimonialimg.jpg" alt="">
                                    <h4>Student Name will be here</h4>
                                    <p class="desi">Software Engineer, Name of Company</p>
                                    <p class="py-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing .</p>
                                    <!--/ image -->
                                </div>
                                <!--/ item -->
                                <!-- item -->
                            <div class="carousel-item">
                                    <!-- image -->
                                    <img src="img/testimonialimg.jpg" alt="">
                                    <h4>Student Name will be here</h4>
                                    <p class="desi">Software Engineer, Name of Company</p>
                                    <p class="py-2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing .</p>
                                    <!--/ image -->
                                </div>
                                <!--/ item -->
                        </div>
                        <a class="carousel-control-prev" href="#testimonials-car" role="button" data-slide="prev">
                            <span class="icon-left-arrow icomoon"></span>
                        </a>
                        <a class="carousel-control-next" href="#testimonials-car" role="button" data-slide="next">
                            <span class="icon-right-arrow icomoon"></span>
                        </a>
                    </div>
                    <!--/ carousel-->
                </div>
                <!--/ col -->
            </div>
        </div>
        <!--/ container -->
    </div>
    <!--/ students testimonials -->   
    
    <!--/ main -->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'footerscripts.php' ?>

</body>
</html>