<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<!-- styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">    
    <link rel="stylesheet" href="css/easy-responsive-tabs.css">
    <link rel="stylesheet" href="css/modal.css">
    <link rel="stylesheet" href="css/fonts.css">
    