<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Testimonials of Students</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!-- sub page -->
    <div class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="breadcumb-overlay"></div>
             <!-- container -->
             <div class="container">
                 <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                         <article class="header-page">
                             <h1>Testimonials</h1>
                             <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat, quis.</p>
                         </article>
                    </div>
                 </div>
             </div>
             <!--/ container -->
        </div>
        <!--/ sub page header -->
        <!-- sub page main-->
        <div class="subpage-main">
            <div class="container">


                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="row testrow">
                            <div class="col-lg-4">
                                <img src="img/testimg01.jpg" alt="" class="img-fluid testimg">
                            </div>
                            <div class="col-lg-8 position-relative">
                                <span class="quoteimg"><img src="img/svg/quotation-mark.svg"></span>
                                <article>
                                        <h5 class="h6 fbold mb-0 pt-2">Student Name will be here</h5>
                                        <small>Senior Software Engineer</small>
                                        <p class="pt-1">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Animi id dolorem corporis ad facilis ducimus voluptatem unde architecto. Excepturi, necessitatibus! necessitatibus!</p>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-6">
                        <div class="row testrow">
                            <div class="col-lg-4">
                                <img src="img/testimg01.jpg" alt="" class="img-fluid testimg">
                            </div>
                            <div class="col-lg-8 position-relative">
                                <span class="quoteimg"><img src="img/svg/quotation-mark.svg"></span>
                                <article>
                                        <h5 class="h6 fbold mb-0 pt-2">Student Name will be here</h5>
                                        <small>Senior Software Engineer</small>
                                        <p class="pt-1">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Animi id dolorem corporis ad facilis ducimus voluptatem unde architecto. Excepturi, necessitatibus! necessitatibus!</p>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="row testrow">
                            <div class="col-lg-4">
                                <img src="img/testimg01.jpg" alt="" class="img-fluid testimg">
                            </div>
                            <div class="col-lg-8 position-relative">
                                <span class="quoteimg"><img src="img/svg/quotation-mark.svg"></span>
                                <article>
                                        <h5 class="h6 fbold mb-0 pt-2">Student Name will be here</h5>
                                        <small>Senior Software Engineer</small>
                                        <p class="pt-1">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Animi id dolorem corporis ad facilis ducimus voluptatem unde architecto. Excepturi, necessitatibus! necessitatibus!</p>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="row testrow">
                            <div class="col-lg-4">
                                <img src="img/testimg01.jpg" alt="" class="img-fluid testimg">
                            </div>
                            <div class="col-lg-8 position-relative">
                                <span class="quoteimg"><img src="img/svg/quotation-mark.svg"></span>
                                <article>
                                        <h5 class="h6 fbold mb-0 pt-2">Student Name will be here</h5>
                                        <small>Senior Software Engineer</small>
                                        <p class="pt-1">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Animi id dolorem corporis ad facilis ducimus voluptatem unde architecto. Excepturi, necessitatibus! necessitatibus!</p>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="row testrow">
                            <div class="col-lg-4">
                                <img src="img/testimg01.jpg" alt="" class="img-fluid testimg">
                            </div>
                            <div class="col-lg-8 position-relative">
                                <span class="quoteimg"><img src="img/svg/quotation-mark.svg"></span>
                                <article>
                                        <h5 class="h6 fbold mb-0 pt-2">Student Name will be here</h5>
                                        <small>Senior Software Engineer</small>
                                        <p class="pt-1">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Animi id dolorem corporis ad facilis ducimus voluptatem unde architecto. Excepturi, necessitatibus! necessitatibus!</p>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->


                </div>
                <!--/ row -->  

                
                
                
            </div>
        </div>
        <!--/sub page main -->
    </div>
    <!--/ sub page -->
    <!-- footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- script files -->
    <?php include 'footerscripts.php' ?>
</body>
</html>